import React from 'react';
import ButtonHome from '../commons/ButtonHome/ButtonHome';

class Reglamento extends React.Component {
  state={
    dias:[]
  }

  constructor () {
    super()
  }

  componentDidMount(){
    this.getCatalogoValues("DAY", this.setDays);
  }

  getCatalogoValues(codigoCatalogo, setTargetStateFn){
    fetch('http://localhost:8080/api/catalogo/findByCodigo/'+codigoCatalogo)
    .then(res=>res.json())
    .then((data)=>{
      let self = this;
      setTargetStateFn(self, data[0].valores);
    })
    .catch(console.log)
  }

  setDays(self, dias){
    self.setState({dias:dias});
  }

  render () {
    return (
      <div className="col-sm-12">
      <ButtonHome />
      <div className="p-4 col-md-10 offset-md-1">
        <div className="card">
            <div className="card-header">Reglamento (Pico y Placa)</div>
            <div className="card-body p-5">
              <FormReglamento dias={this.state.dias}/>
          </div>
        </div>
        </div>
      </div>
    )
  }
}

const FormReglamento = ({dias}) => (
  <div className='text-white row'>
    <div className="input-group row form-group col-sm-12">
      <div className="input-group-prepend">
        <label htmlFor="placa" className="input-group-text">Día</label>
      </div>
        <select className="custom-select" id="marca">
          <option defaultValue disabled>Seleccione...</option>
          {dias.map((dia)=>
            (<option key={dia.id} value={dia.id}>{dia.descripcion}</option>)
          )}
        </select>
    </div>
    <div className="input-group row form-group col-sm-12 col-md-6 mr-1">
      <div className="input-group-prepend">
        <label htmlFor="horaInicio" className="input-group-text">Hora inicio</label>
      </div>
        <input type="time" className="form-control" id="horaInicio" placeholder="horaInicio"/>
    </div>
    <div className="input-group row form-group col-sm-12 col-md-6">
      <div className="input-group-prepend">
        <label htmlFor="horaFin" className="input-group-text">Hora fin</label>
      </div>
        <input type="time" className="form-control" id="horaFin" placeholder="horaFin"/>
    </div>
    <div className="input-group row form-group col-sm-12">
      <div className="input-group-prepend">
        <label htmlFor="horaFin" className="input-group-text">Hora fin</label>
      </div>
        <input type="number" max="9" min="0" className="form-control"
          id="digitoPlaca" placeholder="Ingrese último digito de placa"/>
        <button className="input-group-button btn btn-primary">Añadir</button>
    </div>
  </div>
)

export default Reglamento;
